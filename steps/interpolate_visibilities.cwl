class: CommandLineTool
cwlVersion: v1.0
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
id: interpolate_visibilities

baseCommand:
  - DPPP
arguments:
  - msout.datacolumn=INTERP_DATA
  - msout=.
  - msin.datacolumn=DATA
  - steps=[interpolate]
inputs:
  - id: msin
    type: Directory
    inputBinding:
      prefix: 'msin='
      separate: false
  - id: storage_manager
    type: string
    default: Dysco
    inputBinding:
      separate: false
      prefix: 'msout.storagemanager='
  - id: storage_manager_databitrate
    type: int
    default: 0
    inputBinding:
      prefix: 'msout.storagemanager.databitrate='
      separate: false
  - id: window_size
    type: int
    inputBinding:
      prefix: 'interpolate.windowsize='
      separate: false

outputs:
  - id: msout
    type: Directory
    outputBinding:
      glob: $(inputs.msin.basename)
label: interpolate_visibilities
hints:
  - class: DockerRequirement
    dockerPull: lofareosc/lofar-pipeline-ci:latest

requirements:
  - class: InitialWorkDirRequirement
    listing:
      - entry: $(inputs.msin)
        writable: true
