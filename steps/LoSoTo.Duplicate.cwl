#!/usr/bin/env cwl-runner

class: CommandLineTool
cwlVersion: v1.0
id: losoto_duplicate

doc: |
  Duplicate a table

requirements:
  InlineJavascriptRequirement: {}
  InitialWorkDirRequirement:
    listing:
      - entryname: 'parset.config'
        entry: |
          [duplicate]
          soltab = $(inputs.soltab)
          operation = DUPLICATE
          $(inputs.soltabOut !== null? 'soltabOut=' + inputs.soltabOut: '')

      - entryname: $(inputs.input_h5parm.basename)
        entry: $(inputs.input_h5parm)
        writable: true

baseCommand: "losoto"

arguments:
  - $(inputs.input_h5parm.basename)
  - parset.config

hints:
  DockerRequirement:
    dockerPull: lofareosc/lofar-pipeline-ci:latest

inputs:
  - id: input_h5parm
    type: File
    format: lofar:#H5Parm
  - id: soltab
    type: string
    doc: "Solution table"
  - id: soltabOut
    type: string?
    doc: Output table name. By default choose next available from table type.
outputs:
  - id: output_h5parm
    type: File
    format: lofar:#H5Parm
    outputBinding:
      glob: $(inputs.input_h5parm.basename)


$namespaces:
  lofar: https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
$schema:
  - https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
