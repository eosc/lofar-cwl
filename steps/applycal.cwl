class: CommandLineTool
cwlVersion: v1.0
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
id: applycal
baseCommand:
  - DPPP
inputs:
  - id: msin
    type: Directory
    inputBinding:
      position: 0
      prefix: msin=
      separate: false
    doc: Input Measurement Set
  - default: DATA
    id: msin_datacolumn
    type: string
    inputBinding:
      position: 0
      prefix: msin.datacolumn=
      separate: false
    doc: Input data Column
  - id: parmdb
    type: File
    inputBinding:
      position: 0
      prefix: applycal.parmdb=
      separate: false
    doc: >-
      Path of parmdb in which the parameters are stored. This can also be an
      H5Parm file, in that case the filename has to end in '.h5'
  - id: msout_datacolumn
    type: string
    inputBinding:
      position: 0
      prefix: msout.datacolumn=
      separate: false
    doc: Output data column
  - default: gain
    id: correction
    type: string
    inputBinding:
      position: 0
      prefix: applycal.correction=
      separate: false
    doc: >
      Type of correction to perform. When using H5Parm, this is for now the name
      of the soltab; the type will be deduced from the metadata in that soltab,
      except for full Jones, in which case correction should be 'fulljones'.
  - id: storagemanager
    type: string
    default: dysco
    inputBinding:
      prefix: msout.storagemanager=
  - id: updateweights
    type: boolean?
    inputBinding:
      position: 0
      prefix: -applycal.updateweights=True
outputs:
  - id: msout
    doc: Output Measurement Set
    type: Directory
    outputBinding:
      glob: $(inputs.msin.basename)
arguments:
  - 'steps=[applycal]'
  - msout=.
requirements:
  - class: InitialWorkDirRequirement
    listing:
      - entry: $(inputs.msin)
        writable: true
  - class: InlineJavascriptRequirement
hints:
  - class: DockerRequirement
    dockerPull: 'lofareosc/lofar-pipeline-ci:latest'
