#!/usr/bin/env cwl-runner

class: CommandLineTool
cwlVersion: v1.0
id: losoto_faraday

$namespaces:
  lofar: https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
doc: |
  Faraday rotation extraction from either a rotation table or a
  circular phase (of which the operation get the polarisation difference).


requirements:
  InlineJavascriptRequirement:
    expressionLib:
      - { $include: utils.js}
  InitialWorkDirRequirement:
    listing:
      - entryname: 'parset.config'
        entry: $(get_losoto_config('FARADAY').join('\n'))

      - entryname: $(inputs.input_h5parm.basename)
        entry: $(inputs.input_h5parm)
        writable: true

baseCommand: "losoto"

arguments:
  - $(inputs.input_h5parm.basename)
  - parset.config

hints:
  DockerRequirement:
    dockerPull: lofareosc/lofar-pipeline-ci:latest

inputs:
  - id: input_h5parm
    type: File
    format: lofar:#H5Parm
  - id: soltab
    type: string
    doc: "Solution table"
  - id: refAnt
    type: string?
    doc:  Reference antenna, by default the first.
  - id: maxResidual
    type: float?
    doc: |
      Max average residual in radians before flagging datapoint,
      by default 1. If 0: no check.


outputs:
  - id: output_h5parm
    type: File
    format: lofar:#H5Parm
    outputBinding:
      glob: $(inputs.input_h5parm.basename)
$schema:
  - https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
