#!/usr/bin/env cwl-runner

class: CommandLineTool
cwlVersion: v1.0
id: losoto_splitleak

$namespaces:
  lofar: https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
doc: Duplicate a table

requirements:
  InlineJavascriptRequirement:
    expressionLib:
      - { $include: utils.js}
  InitialWorkDirRequirement:
    listing:
      - entryname: 'parset.config'
        entry: $(get_losoto_config('SPLITLEAK').join('\n'))

      - entryname: $(inputs.input_h5parm.basename)
        entry: $(inputs.input_h5parm)
        writable: true

baseCommand: "losoto"

arguments:
  - $(inputs.input_h5parm.basename)
  - parset.config

hints:
  DockerRequirement:
    dockerPull: lofareosc/lofar-pipeline-ci:latest

inputs:
  - id: input_h5parm
    type: File
    format: lofar:#H5Parm
  - id: soltab
    type: string
    doc: "Solution table"
  - id: soltabOutG
    type: string?
    doc: Output table name (diagonal component). By default choose next available from table type.
  - id: soltabOutD
    type: string?
    doc: Output table name (leakage component). By default choose next available from table type.

outputs:
  - id: output_h5parm
    type: File
    format: lofar:#H5Parm
    outputBinding:
      glob: $(inputs.input_h5parm.basename)
$schema:
  - https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
