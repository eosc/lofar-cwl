#!/usr/bin/env cwl-runner

class: CommandLineTool
cwlVersion: v1.0
id: losoto_prefactor_bandpass

$namespaces:
  lofar: https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
doc: |
  This operation for LoSoTo implements the Prefactor bandpass operation
  WEIGHT: flag-only compliant, no need for weight


requirements:
  InlineJavascriptRequirement:
    expressionLib:
      - { $include: utils.js}
  InitialWorkDirRequirement:
    listing:
      - entryname: 'parset.config'
        entry: $(get_losoto_config('PREFACTOR_BANDPASS').join('\n'))

      - entryname: $(inputs.input_h5parm.basename)
        entry: $(inputs.input_h5parm)
        writable: true

baseCommand: "losoto"

arguments:
  - $(inputs.input_h5parm.basename)
  - parset.config

hints:
  DockerRequirement:
    dockerPull: lofareosc/lofar-pipeline-ci:latest

inputs:
  - id: input_h5parm
    type: File
    format: lofar:#H5Parm
  - id: soltab
    type: string
    doc: "Solution table"
  - id: chanWidth
    type: string?
    doc: |
      the width of each channel in the data from which solutions were obtained. Can be
      either a string like "48kHz" or a float in Hz. If interpolate = True, chanWidth
      must be specified

  - id: BadSBList
    type: string?
    doc: a list of bad subbands that will be flagged

  - id: outSoltabName
    type: string?
    doc: |
      Name of the output bandpass soltab. An existing soltab with this name will be
      overwritten
  - id: interpolate
    type: boolean?
    default: false
    doc: |
      If True, interpolate to a regular frequency grid and then smooth, ignoring bad
      subbands. If False, neither interpolation nor smoothing is done and the output
      frequency grid is the same as the input one. If interpolate = True, chanWidth
      must be specified
  - id: removeTimeAxis
    type: boolean?
    doc: |
      If True, the time axis of the output bandpass soltab is removed by doing a median
      over time. If False, the output time grid is the same as the input one
  - id: autoFlag
    type: boolean?
    doc: If True, automatically flag bad frequencies and stations

  - id: nSigma
    type: float?
    doc: Number of sigma for autoFlagging. Amplitudes outside of nSigma*stddev are flagged

  - id: maxFlaggedFraction
    type: float?
    doc: |
      Maximum allowable fraction of flagged frequencies for autoFlagging. Stations with
      higher fractions will be completely flagged
  - id: maxStddev
    type: float?
    doc: Maximum allowable standard deviation for autoFlagging

  - id: ncpu
    type: int?
    doc: Number of CPUs to use during autoFlagging (0 = all)

outputs:
  - id: output_h5parm
    type: File
    format: lofar:#H5Parm
    outputBinding:
      glob: $(inputs.input_h5parm.basename)
$schema:
  - https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
