#!/usr/bin/env cwl-runner

class: CommandLineTool
cwlVersion: v1.0
id: losoto_prefactor_xyoffset

$namespaces:
  lofar: https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl

requirements:
  InlineJavascriptRequirement:
    expressionLib:
      - { $include: utils.js}
  InitialWorkDirRequirement:
    listing:
      - entryname: 'parset.config'
        entry: $(get_losoto_config('PREFACTOR_XYOFFSET').join('\n'))

      - entryname: $(inputs.input_h5parm.basename)
        entry: $(inputs.input_h5parm)
        writable: true

baseCommand: "losoto"

arguments:
  - $(inputs.input_h5parm.basename)
  - parset.config

hints:
  DockerRequirement:
    dockerPull: lofareosc/lofar-pipeline-ci:latest

inputs:
  - id: input_h5parm
    type: File
    format: lofar:#H5Parm
  - id: soltab
    type: string
    doc: "Solution table"
  - id: chanWidth
    type: string
    doc: |
      the width of each channel in the data from which solutions were obtained. Can be
      either a string like "48kHz" or a float in Hz.
      
outputs:
  - id: output_h5parm
    type: File
    format: lofar:#H5Parm
    outputBinding:
      glob: $(inputs.input_h5parm.basename)
$schema:
  - https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
