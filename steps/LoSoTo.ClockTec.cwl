#!/usr/bin/env cwl-runner

class: CommandLineTool
cwlVersion: v1.0
id: losoto_clocktec

$namespaces:
  lofar: https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
doc: |
  Separate phase solutions into Clock and TEC.
  The Clock and TEC values are stored in the specified output soltab with type 'clock', 'tec', 'tec3rd'.

requirements:
  InlineJavascriptRequirement:
    expressionLib:
      - { $include: utils.js}
  InitialWorkDirRequirement:
    listing:
      - entryname: 'parset.config'
        entry: $(get_losoto_config('CLOCKTEC').join('\n'))

      - entryname: $(inputs.input_h5parm.basename)
        entry: $(inputs.input_h5parm)
        writable: true

baseCommand: "losoto"

arguments:
  - $(inputs.input_h5parm.basename)
  - parset.config

hints:
  DockerRequirement:
    dockerPull: lofareosc/lofar-pipeline-ci:latest

inputs:
  - id: input_h5parm
    type: File
    format: lofar:#H5Parm
  - id: soltab
    type: string
    doc: "Solution table"
  - id: clocksoltabOut
    doc: output soltab name for clock
    type: string?
  - id: tecsoltabOut
    doc: output soltab name for tec
    type: string?
  - id: offsetsoltabOut
    doc: output soltab name for phase offset
    type: string?
  - id: tec3rdsoltabOut
    doc: output soltab name for tec3rd offset
    type: string?
  - id: flagBadChannels
    type: boolean?
    doc: Detect and remove bad channel before fitting, by default True.
  - id: flagCut
    type: float?
  - id: chi2cut
    type: float?
  - id: combinePol
    type: boolean?
    doc: |
      Find a combined polarization solution, by default False.
  - id: removePhaseWraps
    type: boolean?
    doc: |
      Detect and remove phase wraps, by default True.
  - id: fit3rdorder
    type: boolean?
    doc: |
      Fit a 3rd order ionospheric ocmponent (usefult <40 MHz). By default False.
  - id: circular
    type: boolean?
    doc: |
      Assume circular polarization with FR not removed. By default False.
  - id: reverse
    type: boolean?
    doc:
      Reverse the time axis. By default False.
  - id: invertOffset
    type: boolean?
    doc: |
      Invert (reverse the sign of) the phase offsets. By default False. Set to True
      if you want to use them with the residuals operation.

outputs:
  - id: output_h5parm
    type: File
    format: lofar:#H5Parm
    outputBinding:
      glob: $(inputs.input_h5parm.basename)
$schema:
  - https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
