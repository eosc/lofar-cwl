function get_losoto_config(step_name) {
    var par = ['soltab = ' + inputs.soltab]
    if (inputs.ncpu !== null && inputs.ncpu !== undefined) par.push('ncpu='+inputs.ncpu);
    console.log(inputs, par)
    par.push("[" + step_name + "]")
    par.push('operation=' + step_name)
    for(var field_name in inputs){
        if(field_name === 'input_h5parm' ||
           field_name === 'soltab' ||
           field_name === 'ncpu') continue;

        if(inputs[field_name] === null ||
           inputs[field_name] === 'null') continue;
        
        if(inputs[field_name]["class"] !== undefined &&
           (inputs[field_name]["class"] ==="Directory" ||
            inputs[field_name]["class"] ==="File")){
            par.push(field_name+'='+inputs[field_name].path)
        } else {
            par.push(field_name+'='+inputs[field_name])
        }
    }
    return par
}
