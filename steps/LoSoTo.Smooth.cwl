#!/usr/bin/env cwl-runner

class: CommandLineTool
cwlVersion: v1.0
id: losoto_smooth

$namespaces:
  lofar: https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
doc: |
  A smoothing function: running-median on an arbitrary number of axes, running
  polyfit and Savitzky-Golay on one axis, or set all solutions to the
  mean/median value.
  WEIGHT: flag ready.



requirements:
  InlineJavascriptRequirement:
    expressionLib:
      - { $include: utils.js}
  InitialWorkDirRequirement:
    listing:
      - entryname: 'parset.config'
        entry: $(get_losoto_config('SMOOTH').join('\n'))

      - entryname: $(inputs.input_h5parm.basename)
        entry: $(inputs.input_h5parm)
        writable: true

baseCommand: "losoto"

arguments:
  - $(inputs.input_h5parm.basename)
  - parset.config

hints:
  DockerRequirement:
    dockerPull: lofareosc/lofar-pipeline-ci:latest

inputs:
  - id: input_h5parm
    type: File
    format: lofar:#H5Parm
  - id: soltab
    type: string
    doc: "Solution table"
  - id: axesToSmooth
    type: string[]
    doc: Axes used to compute the smoothing function.
  - id: size
    type: int[]?
    doc: |
      Window size for the runningmedian, savitzky-golay, and runningpoly (array
      of same size of axesToSmooth), by default [].
  - id: mode
    default: 'runningmedian'
    type:
      type: enum
      symbols:
        - 'runningmedian'
        - 'runningpoly'
        - 'savitzky-golay'
        - 'mean'
        - 'median'
    doc: Runningmedian or runningpoly or Savitzky-Golay or mean or median (these last two values set all the solutions to the mean/median), by default "runningmedian".
  - id: degree
    type: int?
    doc: Degrees of the polynomia for the runningpoly or savitzky-golay modes, by default 1.
  - id: replace
    type: boolean?
    doc: Flagged data are replaced with smoothed value and unflagged, by default False.
  - id: log
    type: boolean?
    doc: clip is done in log10 space, by default False
  - id: refAnt
    type: string?
    doc: Reference antenna for phases. By default None.

outputs:
  - id: output_h5parm
    type: File
    format: lofar:#H5Parm
    outputBinding:
      glob: $(inputs.input_h5parm.basename)
$schema:
  - https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
