#!/usr/bin/env cwl-runner

class: CommandLineTool
cwlVersion: v1.0
id: losoto_lofarbeam

$namespaces:
  lofar: https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
doc: |
  LOFAR beam prediction

requirements:
  InlineJavascriptRequirement:
    expressionLib:
      - { $include: utils.js}
  InitialWorkDirRequirement:
    listing:
      - entryname: 'parset.config'
        entry: $(get_losoto_config('LOFARBEAM').join('\n'))

      - entryname: $(inputs.input_h5parm.basename)
        entry: $(inputs.input_h5parm)
        writable: true

baseCommand: "losoto"

arguments:
  - $(inputs.input_h5parm.basename)
  - parset.config

hints:
  DockerRequirement:
    dockerPull: lofareosc/lofar-pipeline-ci:latest

inputs:
  - id: input_h5parm
    type: File
    format: lofar:#H5Parm
  - id: soltab
    type: string
    doc: "Solution table"
  - id: ms
    type: Directory
    doc: "Measurement set"
  - id: inverse
    type: boolean?
    default: false
  - id: useElementResponse
    type: boolean?
    default: true
  - id: useArrayFactor
    type: boolean?
    default: true
  - id: useChanFreq
    type: boolean?
    default: true
outputs:
  - id: output_h5parm
    type: File
    format: lofar:#H5Parm
    outputBinding:
      glob: $(inputs.input_h5parm.basename)
  - id: config
    type: File
    outputBinding:
      glob: "*.config"
$schema:
  - https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
