#!/usr/bin/env cwl-runner

class: CommandLineTool
cwlVersion: v1.0
id: losoto_flagextend

$namespaces:
  lofar: https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
doc: |
    This operation for LoSoTo implement a extend flag procedure.
    It can work in multi dimensional space and for each datum check if the
     surrounding data are flagged to a certain %, then flag also that datum.
    The size of the surrounding footprint can be tuned

requirements:
  InlineJavascriptRequirement:
    expressionLib:
      - { $include: utils.js}
  InitialWorkDirRequirement:
    listing:
      - entryname: 'parset.config'
        entry: $(get_losoto_config('FLAGEXTEND').join('\n'))

      - entryname: $(inputs.input_h5parm.basename)
        entry: $(inputs.input_h5parm)
        writable: true

baseCommand: "losoto"

arguments:
  - $(inputs.input_h5parm.basename)
  - parset.config

hints:
  DockerRequirement:
    dockerPull: lofareosc/lofar-pipeline-ci:latest

inputs:
  - id: input_h5parm
    type: File
    format: lofar:#H5Parm
  - id: soltab
    type: string
    doc: "Solution table"
  - id: axesToExt
    type: string[]
    doc: Axes used to find close flags.
  - id: size
    type: int[]
    doc: |
      Size of the window (diameter), per axis. If 0 is given then the entire
       length of the axis is assumed.
      Must be a vector of same length of Axes.
  - id: percent
    type: float?
    doc: Percent of flagged data around the point to flag it, by default 50.
  - id: maxCycles
    type: int?
    doc: Number of independent cycles of flag expansion, by default 3.
  - id: ncpu
    type: int?
    doc: Number of CPU used, by default all available.

outputs:
  - id: output_h5parm
    type: File
    format: lofar:#H5Parm
    outputBinding:
      glob: $(inputs.input_h5parm.basename)
$schema:
  - https://git.astron.nl/eosc/ontologies/raw/master/schema/lofar.owl
