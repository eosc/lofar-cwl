class: ExpressionTool
cwlVersion: v1.0
id: fileselector
inputs:
  - id: inputs
    type: Any
    doc: input files
  - id: step_name
    type: string
    doc: >-
      id of the step that generates the output file or directory
      (e.g. 'demixstepgenerator')
  - id: key_name
    type: string
    doc: >-
      key of the parset that contains the name of the outputfile
      (e.g. 'instrumentmodel')
outputs:
  - id: output
    type: File

expression: |
  ${
    var step_name = inputs.step_name
    var key_name = inputs.key_name
    return {'output': inputs.inputs[step_name][key_name]}
  }
label: FileSelector

requirements:
  - class: InlineJavascriptRequirement
