#!/usr/bin/env cwl-runner

class: CommandLineTool
cwlVersion: v1.0
id: aoflagger

requirements:
  - class: InlineJavascriptRequirement
  - class: InitialWorkDirRequirement
    listing:
      - entry: $(inputs.msin)
        writable: true

hints:
  DockerRequirement:
    dockerPull: lofareosc/lofar-pipeline-ci:latest

baseCommand:
  - aoflagger
inputs:
  - id: msin
    type: Directory[]
    doc: Input measurement set
    inputBinding:
      position: 0
  - id: verbose
    type: boolean?

    doc: will produce verbose output
    inputBinding:
      valueFrom: -v
  - id: n_threads
    type: int?
    doc: |
      overrides the number of threads specified in the strategy
      (default: one thread for each CPU core)
    inputBinding:
      prefix: -j

  - id: strategy
    type: File?
    doc: |
      specifies a customized strategy
    inputBinding:
      prefix: -strategy

#### READ MODES
  - id: direct_read
    type: boolean?
    doc: |
      Will perform the slowest IO but will always work.
    inputBinding:
      prefix: -direct-read
  - id: indirect_read
    type: boolean?
    doc: |
      Will reorder the measurement set before starting, which is normally
      faster but requires free disk space to reorder the data to.
    inputBinding:
      prefix: -indirect-read
  - id: memory_read
    type: boolean?
    doc: |
      Will read the entire measurement set in memory. This is the fastest,
      but requires much memory.
    inputBinding:
      prefix: -memory-read
  - id: auto_read
    type: boolean?
    doc: |
       Will select either memory or direct mode based on available memory (default).
    inputBinding:
      prefix: -auto-read
##############
  - id: skip_flagged
    type: boolean?
    doc: |
      Will skip an ms if it has already been processed by AOFlagger according
      to its HISTORY table.
    inputBinding:
      prefix: -skip-flagged
  - id: uvw
    type: File?
    doc: Reads uvw values (some exotic strategies require these)
    inputBinding:
      prefix: -uvw
  - id: column
    type: string?
    doc: Specify column to flag
    inputBinding:
      prefix: -column
  - id: bands
    type: int[]?
    doc: Comma separated list of (zero-indexed) band ids to process
    inputBinding:
      prefix: -bands
      itemSeparator: ','
  - id: fields
    type: int[]?
    doc: Comma separated list of (zero-indexed) field ids to process
    inputBinding:
      prefix: -fields
      itemSeparator: ','
  - id: combine-spws
    type: boolean?
    doc: Join all SPWs together in frequency direction before flagging
    inputBinding:
      prefix: -combine-spws
  - id: bandpass
    type: File?
    doc: Set bandpass correction file for any 'Apply passband' action
    inputBinding:
      prefix: bandpass

outputs:
  - id: output_ms
    doc: Output image
    type: Directory[]
    outputBinding:
      outputEval: $(inputs.msin)
label: AOFlagger
